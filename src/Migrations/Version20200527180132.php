<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200527180132 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE filiere CHANGE classe_id classe_id INT DEFAULT NULL, CHANGE faculte_id faculte_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE classe CHANGE faculte_id faculte_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE utilisateur CHANGE role_id role_id INT DEFAULT NULL, CHANGE filename filename VARCHAR(255) DEFAULT NULL, CHANGE nom nom VARCHAR(255) DEFAULT NULL, CHANGE prenom prenom VARCHAR(255) DEFAULT NULL, CHANGE telephone telephone VARCHAR(255) DEFAULT NULL, CHANGE date_naissance date_naissance VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE candidature CHANGE filiere_id filiere_id INT DEFAULT NULL, CHANGE statut statut VARCHAR(100) DEFAULT NULL, CHANGE notebac notebac VARCHAR(255) DEFAULT NULL, CHANGE notel3 notel3 VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE jour CHANGE planning_id planning_id INT DEFAULT NULL, CHANGE nom nom VARCHAR(20) NOT NULL, CHANGE date date VARCHAR(100) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE candidature CHANGE filiere_id filiere_id INT DEFAULT NULL, CHANGE statut statut VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE notebac notebac VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE notel3 notel3 VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE classe CHANGE faculte_id faculte_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE filiere CHANGE classe_id classe_id INT DEFAULT NULL, CHANGE faculte_id faculte_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE jour CHANGE planning_id planning_id INT DEFAULT NULL, CHANGE nom nom VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE date date VARCHAR(50) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE utilisateur CHANGE role_id role_id INT DEFAULT NULL, CHANGE filename filename VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE nom nom VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE prenom prenom VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE telephone telephone VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE date_naissance date_naissance VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}
