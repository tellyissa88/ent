<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200527173338 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE jour DROP FOREIGN KEY FK_DA17D9C5122EEC90');
        $this->addSql('DROP TABLE semaine');
        $this->addSql('ALTER TABLE candidature CHANGE filiere_id filiere_id INT DEFAULT NULL, CHANGE statut statut VARCHAR(100) DEFAULT NULL, CHANGE notebac notebac VARCHAR(255) DEFAULT NULL, CHANGE notel3 notel3 VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE classe CHANGE faculte_id faculte_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE filiere CHANGE classe_id classe_id INT DEFAULT NULL, CHANGE faculte_id faculte_id INT DEFAULT NULL');
        $this->addSql('DROP INDEX IDX_DA17D9C5122EEC90 ON jour');
        $this->addSql('ALTER TABLE jour ADD planning_id INT DEFAULT NULL, DROP semaine_id, DROP heure');
        $this->addSql('ALTER TABLE jour ADD CONSTRAINT FK_DA17D9C53D865311 FOREIGN KEY (planning_id) REFERENCES planning (id)');
        $this->addSql('CREATE INDEX IDX_DA17D9C53D865311 ON jour (planning_id)');
        $this->addSql('ALTER TABLE planning CHANGE filiere_id filiere_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE utilisateur CHANGE role_id role_id INT DEFAULT NULL, CHANGE filename filename VARCHAR(255) DEFAULT NULL, CHANGE nom nom VARCHAR(255) DEFAULT NULL, CHANGE prenom prenom VARCHAR(255) DEFAULT NULL, CHANGE telephone telephone VARCHAR(255) DEFAULT NULL, CHANGE date_naissance date_naissance VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE semaine (id INT AUTO_INCREMENT NOT NULL, planning_id INT DEFAULT NULL, INDEX IDX_7B4D8BEA3D865311 (planning_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE semaine ADD CONSTRAINT FK_7B4D8BEA3D865311 FOREIGN KEY (planning_id) REFERENCES planning (id)');
        $this->addSql('ALTER TABLE candidature CHANGE filiere_id filiere_id INT DEFAULT NULL, CHANGE statut statut VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE notebac notebac VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE notel3 notel3 VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE classe CHANGE faculte_id faculte_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE filiere CHANGE classe_id classe_id INT DEFAULT NULL, CHANGE faculte_id faculte_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE jour DROP FOREIGN KEY FK_DA17D9C53D865311');
        $this->addSql('DROP INDEX IDX_DA17D9C53D865311 ON jour');
        $this->addSql('ALTER TABLE jour ADD semaine_id INT DEFAULT NULL, ADD heure DATETIME NOT NULL, DROP planning_id');
        $this->addSql('ALTER TABLE jour ADD CONSTRAINT FK_DA17D9C5122EEC90 FOREIGN KEY (semaine_id) REFERENCES semaine (id)');
        $this->addSql('CREATE INDEX IDX_DA17D9C5122EEC90 ON jour (semaine_id)');
        $this->addSql('ALTER TABLE planning CHANGE filiere_id filiere_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE utilisateur CHANGE role_id role_id INT DEFAULT NULL, CHANGE filename filename VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE nom nom VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE prenom prenom VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE telephone telephone VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE date_naissance date_naissance VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}
