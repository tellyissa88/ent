<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CandidatureRepository")
 */
class Candidature
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Utilisateur", inversedBy="candidatures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $statut;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $notebac;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $notel3;
     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Filiere", inversedBy="candidatures")
     */
    private $filiere;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?Utilisateur
    {
        return $this->user;
    }

    public function setUser(?Utilisateur $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(?string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getNotebac(): ?string
    {
        return $this->notebac;
    }

    public function setNotebac(string $notebac): self
    {
        $this->notebac = $notebac;

        return $this;
    }

    public function getNotel3(): ?string
    {
        return $this->notel3;
    }

    public function setNotel3(string $notel3): self
    {
        $this->notel3 = $notel3;

        return $this;
    }
    public function getFiliere(): ?Filiere
    {
        return $this->filiere;
    }

    public function setFiliere(?Filiere $filiere): self
    {
        $this->filiere = $filiere;

        return $this;
    }
}
