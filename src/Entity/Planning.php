<?php

namespace App\Entity;

use App\Repository\PlanningRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlanningRepository::class)
 */
class Planning
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=Jour::class, mappedBy="planning")
     */
    private $jour;

    public function __construct()
    {
        $this->jour = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Jour[]
     */
    public function getJour(): Collection
    {
        return $this->jour;
    }

    public function addJour(Jour $jour): self
    {
        if (!$this->jour->contains($jour)) {
            $this->jour[] = $jour;
            $jour->setPlanning($this);
        }

        return $this;
    }

    public function removeJour(Jour $jour): self
    {
        if ($this->jour->contains($jour)) {
            $this->jour->removeElement($jour);
            // set the owning side to null (unless already changed)
            if ($jour->getPlanning() === $this) {
                $jour->setPlanning(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return strval($this->id);
    }
}
