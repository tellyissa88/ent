<?php

namespace App\Controller;
use App\Repository\RoleRepository;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(RoleRepository $roleRepository): Response
    {
        return $this->render('home/index.html.twig', [
            
        ]);
    }

    
}
