<?php

namespace App\Controller;

use App\Entity\Classe;
use App\Entity\Filiere;
use App\Entity\Candidature;
use App\Form\CandidatureType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class CandidatureController extends AbstractController
{
    /**
     * @Route("/filiere/{id}/candidature", name="candidature")
     */
    public function candidature(Filiere $filiere, Request $request)
    {
        $candidature = new Candidature();
        $form = $this->createForm(CandidatureType::class, $candidature);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $candidature->setFiliere($filiere)
                        ->setUser($this->getUser())
                        ->setStatut('Transmis');
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($candidature);
            $entityManager->flush();

            return $this->redirectToRoute('filiere_index');
        }

        return $this->render('candidature/candidature.html.twig', [
            'filiere' =>$filiere,
            'form' =>$form->createView()
        ]);
    }
     /**
     * @Route("/mes-candidature", name="mescandidature")
     */
    public function mescandidature()
    {
     $user =  $this->getUser()->getId(); 
    $em = $this->getDoctrine();
    $candidas=$em->getRepository(Candidature::class)->findCandidature($user);
    return $this->render('candidature/voircandidature.html.twig', [
            'candidatures' =>$candidas
        ]);
    }

}
