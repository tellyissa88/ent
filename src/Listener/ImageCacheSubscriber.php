<?php
namespace App\Listener;

use App\Entity\Utilisateur;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class ImageCacheSubscriber implements EventSubscriber{
    /**
     * Undocumented variable
     *
     * @var [type]
     */
     private $cacheManager;
     /**
      * Undocumented variable
      *
      * @var [type]
      */
     private $uploaderHelper;
    public function __construct(CacheManager $cacheManager, UploaderHelper $uploaderHelper)
    {
        
    }
    public function getSubscribedEvents(){

        return [
            'preRemove',
            'preUpdate'
        ];
    }
    public function preRemove(LifecycleEventArgs $args){
        $entity = $args->getEntity();
        if (!$entity instanceof Utilisateur){
            return;
        }
     
        $this->cacheManager->remove($this->uploaderHelper->asset($entity,'imageFile'));

    }
    public function preUpdate(PreUpdateEventArgs $args){
        $entity = $args->getEntity();
        if (!$entity instanceof Utilisateur){
            return;
        }
        if ($entity->getImageFile() instanceof UploadedFile){
            $this->cacheManager->remove($this->uploaderHelper->asset($entity,'imageFile'));
    }
}
}